import mongoose from "mongoose";

const Schema = mongoose.Schema;

// OrderItem Schema

const OrderItemSchema = new Schema({
  quantity: {
    type: Number,
    required: true,
  },
  product: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "product",
  },
});

OrderItemSchema.virtual("id").get(function () {
  return this._id.toHexString();
});

OrderItemSchema.set("toJSON", {
  virtuals: true,
});

const OrderItem = mongoose.model("orderItem", OrderItemSchema);

export default OrderItem;
