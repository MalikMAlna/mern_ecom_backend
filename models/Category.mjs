import mongoose from "mongoose";

const Schema = mongoose.Schema;

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true,
  },
  color: {
    type: String,
  },
  icon: {
    type: String,
  },
  // No Image?
  //   image: {
  //     type: String,
  //   },
});

const Category = mongoose.model("category", CategorySchema);

export default Category;
