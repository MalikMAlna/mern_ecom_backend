import express from "express";
const router = express.Router();
import Order from "../../models/Order.mjs";

// @route  GET `${api_route}/orders`
// @desc   Get List Of All Orders
// @access Private
router.get("/", async (req, res) => {
  const orderList = await Order.find();
  if (!orderList) {
    res.status(500).json({ success: false, msg: "No Orders Found!" });
  }
  res.status(200).send(orderList);
});

// @route  POST `${api_route}/orders`
// @desc   Create An Order
// @access Private
router.post("/", async (req, res) => {
  const newOrderInfo = req.body;
  let newOrder = new Order({
    orderItems: newOrderInfo.orderItems,
    shippingAddress1: newOrderInfo.shippingAddress1,
    shippingAddress2: newOrderInfo.shippingAddress2,
    city: newOrderInfo.city,
    state: newOrderInfo.state,
    zip: newOrderInfo.zip,
    country: newOrderInfo.country,
    phone: newOrderInfo.phone,
    status: newOrderInfo.status,
    totalPrice: newOrderInfo.totalPrice,
    user: newOrderInfo.user,
  });
  newOrder = await newOrder.save();
  if (!newOrder) {
    return res.status(404).send("The order cannot be created!");
  }
  res.send(newOrder);
});

export default router;
