import User from "../../models/User.mjs";
import express from "express";
const router = express.Router();
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

// @route  GET `${api_route}/users`
// @desc   Get List Of All Users
// @access Private?
router.get("/", async (req, res) => {
  const userList = await User.find().select("-passwordHash");

  if (!userList) {
    res.status(500).json({ success: false });
  }
  res.send(userList);
});

// @route  GET `${api_route}/users`
// @desc   Get A User
// @access Private?
router.get("/:id", async (req, res) => {
  const selectedUser = req.params.id;
  const user = await User.findById(selectedUser).select("-passwordHash");
  if (!user) {
    res.status(500).json({ msg: `User with ID: ${selectedUser} not found!` });
  }
  res.status(200).send(user);
});

// @route  POST `${api_route}/users/login`
// @desc   Login A User
// @access Public
router.post("/login", async (req, res) => {
  const user = await User.findOne({ email: req.body.email });
  const secret = process.env.secret;
  if (!user) {
    return res.status(400).send("User not found!");
  }

  if (user && bcrypt.compareSync(req.body.passwordHash, user.passwordHash)) {
    const token = jwt.sign(
      {
        userId: user.id,
        isAdmin: user.isAdmin,
      },
      secret,
      { expiresIn: "1d" }
    );
    return res.status(200).send({ user: user.email, token: token });
  } else {
    return res.status(400).send("Incorrect Email/Password");
  }
});

// @route  POST `${api_route}/users`
// @desc   Create A User
// @access Public
router.post("/register", async (req, res) => {
  const newUserInfo = req.body;
  let newUser = new User({
    name: newUserInfo.name,
    email: newUserInfo.email,
    passwordHash: bcrypt.hashSync(newUserInfo.passwordHash, 13),
    phone: newUserInfo.phone,
    isAdmin: newUserInfo.isAdmin,
    apartment: newUserInfo.apartment,
    zip: newUserInfo.zip,
    city: newUserInfo.city,
    state: newUserInfo.state,
    country: newUserInfo.country,
  });
  newUser = await newUser.save();
  if (!newUser) {
    return res.status(404).send("The user was not successfully created!");
  }
  res.send(newUser);
});

// @route  GET `${api_route}/users/get/count`
// @desc   Get Count Of All Users
// @access Private
router.get("/get/count", async (req, res) => {
  const userCount = await User.countDocuments((count) => count);

  if (!userCount) {
    res.status(500).json({ success: false });
  }
  res.send({ userCount: userCount });
});

// @route  DELETE `${api_route}/products`
// @desc   Delete A Product
// @access Private
router.delete("/:id", (req, res) => {
  const deletedUserId = req.params.id;
  User.findByIdAndRemove(deletedUserId).then((user) => {
    if (user) {
      return res.status(200).json({
        success: true,
        msg: `User with ID: ${deletedUserId} successfully removed!`,
      });
    } else {
      return res.status(404).json({
        success: false,
        msg: `User with ID: ${deletedUserId} failed to be removed!`,
      });
    }
  });
});

export default router;
