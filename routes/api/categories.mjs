import express from "express";
const router = express.Router();
import Category from "../../models/Category.mjs";

// @route  GET `${api_route}/categories`
// @desc   Get List Of All Categories
// @access Public
router.get("/", async (req, res) => {
  const categoryList = await Category.find().sort({ name: -1 });
  if (!categoryList) {
    res.status(500).json({ success: false, msg: "No Categories Found!" });
  }
  res.status(200).send(categoryList);
});

// @route  GET `${api_route}/categories`
// @desc   Get A Category
// @access Public
router.get("/:id", async (req, res) => {
  const selectedCategory = req.params.id;
  const category = await Category.findById(selectedCategory);
  if (!category) {
    res
      .status(500)
      .json({ msg: `Category with ID: ${selectedCategory} not found!` });
  }
  res.status(200).send(category);
});

// @route  PUT `${api_route}/categories`
// @desc   Update A Category
// @access Private
router.put("/:id", async (req, res) => {
  const selectedCategory = req.params.id;
  const selectedCategoryInfo = req.body;

  const category = await Category.findByIdAndUpdate(
    selectedCategory,
    {
      name: selectedCategoryInfo.name,
      color: selectedCategoryInfo.color,
      icon: selectedCategoryInfo.icon,
    },
    // Returns Updated Category Data Instead of Old Category Data
    { new: true }
  );

  if (!category) {
    res
      .status(400)
      .send(`Category with ID: ${selectedCategory} cannot be updated!`);
  }
  res.status(200).send(category);
});

// @route  POST `${api_route}/categories`
// @desc   Create A Category
// @access Private?
router.post("/", async (req, res) => {
  const newCategoryInfo = req.body;
  let newCategory = new Category({
    name: newCategoryInfo.name,
    color: newCategoryInfo.color,
    icon: newCategoryInfo.icon,
  });
  newCategory = await newCategory.save();
  if (!newCategory) {
    return res.status(404).send("The category cannot be created!");
  }
  res.send(newCategory);
});

// @route  DELETE `${api_route}/categories`
// @desc   Delete A Category
// @access Private?
router.delete("/:id", (req, res) => {
  const deletedCategory = req.params.id;
  Category.findByIdAndRemove(deletedCategory)
    .then((category) => {
      if (category) {
        return res.status(200).json({
          success: true,
          msg: `Category with ID: ${deletedCategory} successfully removed!`,
        });
      } else {
        return res.status(404).json({
          success: false,
          msg: `Category with ID: ${deletedCategory} failed to be removed!`,
        });
      }
    })
    .catch((err) => {
      return res.status(400).json({ success: false, error: err });
    });
});

export default router;
