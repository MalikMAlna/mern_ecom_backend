import express from "express";
import Category from "../../models/Category.mjs";
const router = express.Router();
import mongoose from "mongoose";
import Product from "../../models/Product.mjs";

// @route  GET `${api_route}/products`
// @desc   Get List Of All Products
// @access Public
router.get("/", (req, res) => {
  let filter = {};
  if (req.query.categories) {
    filter = { category: req.query.categories.split(",") };
  }

  Product.find(filter)
    .populate("category")
    // Use Select To Get Back Specific Properties More Efficiently From Product Object
    // .select("name image -_id")
    .sort({ dateAdded: -1 })
    .then((products) => res.json(products))
    .catch(() =>
      res.status(500).json({
        success: false,
        msg: "No Products Found!",
      })
    );
});

// @route  GET `${api_route}/products`
// @desc   Get A Product
// @access Public
router.get("/:id", (req, res) => {
  const selectedProduct = req.params.id;
  Product.findById(selectedProduct)
    .populate("category")
    .then((product) => res.json(product))
    .catch(() =>
      res.status(500).json({
        success: false,
        msg: "Product Not Found!",
      })
    );
});

// @route  POST `${api_route}/products`
// @desc   Create A Product
// @access Private
router.post("/", async (req, res) => {
  const newProductInfo = req.body;
  const category = await Category.findById(newProductInfo.category);

  if (!category) {
    return res.status(400).send("Invalid Category!");
  }

  const newProduct = new Product({
    name: newProductInfo.name,
    shortDescription: newProductInfo.shortDescription,
    longDescription: newProductInfo.longDescription,
    image: newProductInfo.image,
    // imagesGallery: newProductInfo.imagesGallery,
    brand: newProductInfo.brand,
    price: newProductInfo.price,
    category: newProductInfo.category,
    countInStock: newProductInfo.countInStock,
    rating: newProductInfo.rating,
    numReviews: newProductInfo.numReviews,
    isFeatured: newProductInfo.isFeatured,
  });
  newProduct
    .save()
    .then((createdProduct) => {
      res.status(201).json(createdProduct);
    })
    .catch((err) => {
      res.status(500).json({
        error: err,
        success: false,
      });
    });
});

// @route  PUT `${api_route}/categories`
// @desc   Update A Category
// @access Private
router.put("/:id", async (req, res) => {
  const selectedProduct = req.params.id;
  if (!mongoose.isValidObjectId(selectedProduct)) {
    res.status(400).send("Invalid Product ID!");
  }
  const selectedProductInfo = req.body;

  const category = await Category.findById(selectedProductInfo.category);

  if (!category) {
    return res.status(400).send("Invalid Category!");
  }

  const product = await Product.findByIdAndUpdate(
    selectedProduct,
    {
      name: selectedProductInfo.name,
      shortDescription: selectedProductInfo.shortDescription,
      longDescription: selectedProductInfo.longDescription,
      image: selectedProductInfo.image,
      // imagesGallery: selectedProductInfo.imagesGallery,
      brand: selectedProductInfo.brand,
      price: selectedProductInfo.price,
      category: selectedProductInfo.category,
      countInStock: selectedProductInfo.countInStock,
      rating: selectedProductInfo.rating,
      numReviews: selectedProductInfo.numReviews,
      isFeatured: selectedProductInfo.isFeatured,
    },
    // Returns Updated Product Data Instead of Old Product Data
    { new: true }
  );
  if (!product) {
    res
      .status(500)
      .send(`Product with ID: ${selectedProduct} cannot be updated!`);
  }
  res.status(200).send(product);
});

// @route  DELETE `${api_route}/products`
// @desc   Delete A Product
// @access Private
router.delete("/:id", (req, res) => {
  const deletedProduct = req.params.id;
  if (!mongoose.isValidObjectId(deletedProduct)) {
    res.status(400).send("Invalid Product ID!");
  }
  Product.findById(deletedProduct)
    .then((product) =>
      product.remove().then(() =>
        res.json({
          result: `Product with ID: ${deletedProduct} successfully removed!`,
        })
      )
    )
    .catch(() =>
      res.status(404).json({
        result: `Product with ID: ${deletedProduct} failed to be removed!`,
      })
    );
});

// @route  GET `${api_route}/products/get/count`
// @desc   Get A Count of All Products
// @access Public
router.get("/get/count", async (req, res) => {
  const productCount = await Product.countDocuments((count) => count);

  if (!productCount) {
    res.status(500).json({ success: false });
  }
  res.send({ productCount: productCount });
});

// @route  GET `${api_route}/products/get/featured`
// @desc   Get All Featured Products
// @access Public
router.get("/get/featured/:count", async (req, res) => {
  const count = req.params.count ? req.params.count : 0;
  const products = await Product.find({ isFeatured: true }).limit(+count);

  if (!products) {
    res.status(500).json({ success: false });
  }
  res.send(products);
});

export default router;
