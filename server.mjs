import express from "express";
import morgan from "morgan";
import mongoose from "mongoose";
import path from "path";
import cors from "cors";

import "dotenv/config.js";

// Helper Functions
import authJwt from "./helpers/jwt.mjs";
import errorHandler from "./helpers/errorHandler.mjs";

// Routers
import productsRouter from "./routes/api/products.mjs";
import categoriesRouter from "./routes/api/categories.mjs";
import usersRouter from "./routes/api/users.mjs";

// Server App
const app = express();
const api_route = process.env.API_URL;

// CORS Access
app.use(cors());
app.options("*", cors());

// body-parser/MiddleWare
app.use(express.json());
app.use(morgan("tiny"));
app.use(authJwt());
app.use(errorHandler);

const db = process.env.mongoURI;

mongoose
  .connect(db, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  })
  .then(() => {
    console.log("MongoDB successfully conneted!");
  })
  .catch((err) => {
    console.log(err);
  });

// API Routes
app.use(`${api_route}/products`, productsRouter);
app.use(`${api_route}/categories`, categoriesRouter);
app.use(`${api_route}/users`, usersRouter);

// Serve static assets if in production
if (process.env.NODE_ENV === "production") {
  // Sets the static folder
  app.use(express.static("client/build"));

  app.get("*", (req, res) => {
    res.sendFile(path.resolve(__dirname, "client", "build", "index.html"));
  });
}

const port = process.env.PORT || 3000;

app.listen(port, () => console.log(`Server is running on port ${port}`));
